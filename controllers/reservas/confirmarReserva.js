const getDB = require("../../bbdd/getDB");

const query = `UPDATE reservas SET confirmed = true where id = ?`;
const confirmarReserva = async (req, res, next) => {
  let connection;
  try {
    const { idReserva } = req.params;
    connection = await getDB();
    await connection.query(query, [idReserva]);
    res.send({
      status: 200,
    });
  } catch (error) {
    console.error(error);
    res.send({
      status: 500,
      message: "La reserva no puede ser editada",
    });
  } finally {
    if (connection) connection.release();
  }
};

module.exports = confirmarReserva;
