const nuevaReserva = require("./nuevaReserva");
const confirmarReserva = require("./confirmarReserva");
/* const editoReserva = require("./editoReserva"); 
const infoReserva = require("./infoReserva");
const reservas = require("./reservas");
const borroReserva = require("./borroReserva"); */
module.exports = {
  nuevaReserva,
  confirmarReserva,
};
