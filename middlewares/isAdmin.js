//DEVOLVERME SI ES TRUE O FALSE(ADMIN O NO)

const jwt = require("jsonwebtoken");

const userAuth = async (req, res, next) => {
  try {
    // Obtenemos la cabecera de autorización.
    const { userAuth } = req;

    // Si no existe token de usuario lanzamos un error.
    if (!userAuth) {
      const error = new Error("Falta el token de usuario");
      error.httpStatus = 401;
      throw error;
    }

    try {
      const userRole = req.userAuth.role;
      console.log(userRole, "userRole");
      if (userRole === "admin") next();
      throw new Error();
    } catch (_) {
      const error = new Error("El usuario no es admin");
      error.httpStatus = 401;
      throw error;
    }
    next();
  } catch (error) {
    next(error);
  }
};

module.exports = userAuth;
